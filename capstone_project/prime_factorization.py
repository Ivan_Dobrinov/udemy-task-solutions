def is_prime(num):
    for i in range(2, num):
        if num % i == 0:
            return False

    return True


n = int(input("Enter number: "))
nums_to_print = [x for x in range(2, n) if is_prime(x) and n % x == 0]

print("Factors are:")
print(', '.join(map(lambda x: str(x), nums_to_print)))

