def factorial_recursion(n):
    if n == 1:
        return n
    return n * factorial_recursion(n - 1)


def factorial_loop(n):
    factorial = 1
    while n > 1:
        factorial *= n
        n -= 1

    return factorial


n = int(input('Enter a number: '))

print("Recursion:")
print(factorial_recursion(n))
print("Loop:")
print(factorial_loop(n))
