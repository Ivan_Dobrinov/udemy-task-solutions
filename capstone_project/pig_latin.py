vowels = 'AEIOU'
vowels += vowels.lower()

while True:
    word = input('Enter a word (type "quit" to quit):\n')
    if word.lower() == 'quit':
        quit()

    if word[0] not in vowels:
        word = word[1::] + word[0] + 'ay'
    else:
        word += 'ay'
    print('Translated to pig latin =', word)
