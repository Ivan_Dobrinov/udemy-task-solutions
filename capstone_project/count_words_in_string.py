def get_count(string, is_file=False):
    if is_file:
            with open(string) as file:
                return len(file.read().split())
    else:
        return len(string.split())


c = input("File or direct string? (file/string)\n")

if c == 'file':
    print('Word count:', get_count(input('Enter file name:\n'), True))
else:
    print('Word count:', get_count(input('Enter string: \n'), False))
