def rev(s):
    out = ''
    for c in reversed(s):
        out += c
    return out


s = input("Enter string:\n")
print(rev(s))
