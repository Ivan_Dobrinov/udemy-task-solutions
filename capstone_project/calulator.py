def calculator():
    n1 = int(input('Enter first number: '))
    n2 = int(input('Enter second number: '))

    op = input('Chose what operation you would like to perform\n(+, -, *, /, ^)')

    if op == '+':
        return n1 + n2
    elif op == '-':
        return n1 - n2
    elif op == '*':
        return n1 * n2
    elif op == '/':
        return n1 / n2


print(calculator())