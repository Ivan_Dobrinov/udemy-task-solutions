import random

coin = ['heads', 'tails']

flips = int(input('How many flips?\n'))

heads = 0
tails = 0

for i in range(flips):
    face = coin[random.randint(0, 1)]
    print(face)
    if face == coin[0]:
        heads += 1
    else:
        tails += 1


print('Heads:', heads)
print('Tails:', tails)