def is_prime(num):
    for i in range(2, num):
        if num % i == 0:
            return False

    return True


def next_prime(curr):
    while not is_prime(curr):
        curr += 1
    return curr

curr = 2
while True:
    print(curr)
    c = input("See next prime? Y/n ")
    if c.lower() == 'y':
        curr = next_prime(curr + 1)
    else:
        break
