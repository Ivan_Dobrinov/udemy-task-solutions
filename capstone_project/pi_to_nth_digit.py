import math, sys
from decimal import *

sys.setrecursionlimit(100000)


def factorial(x):
    if not x:
        return 1
    return x * factorial(x - 1)


def get_down(k):
    k = k + 1
    getcontext().prec = k
    sum = 0
    for i in range(k):
        up = factorial(6 * i) * (13591409 + 545140134 * i)
        down = factorial(3 * i) * (factorial(i)) ** 3 * (640320 ** (3 * i))
        sum += up / down

    return Decimal(sum)


def get_pi(k):
    down = get_down(k)
    up = 426880 * math.sqrt(10005)
    pi = Decimal(up) / down

    return pi


k = int(input("How many digits would you like?\n"))

print(get_pi(k))
