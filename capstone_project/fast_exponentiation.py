def pow(a, b):
    if b == 0:
        return 1
    elif b == 1:
        return a
    elif b % 2 == 0:
        return pow(a * a, b / 2)
    else:
        return a * pow(a * a, (b - 1) / 2)


a = int(input("Enter number a:\n"))
b = int(input("Enter exponent b:\n"))

print("a^b =", pow(a, b))
