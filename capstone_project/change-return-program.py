import re

penny = 0.01
nickel = 0.05
dime = 0.10
quarter = 0.25

coins = {'quarter': 0, "dime": 0, "nickel": 0, "penny": 0}

cost = float(input("Enter cost:\n"))
money = float(input("Enter money given:\n"))

change = money - cost

for coin in coins.keys():
    while change - globals()[coin] >= 0:
        change -= globals()[coin]
        coins[coin] += 1

print("For the change you will need:\n")
print('\n'.join([': '.join((x, y.__str__())) for x, y in coins.items()]))
