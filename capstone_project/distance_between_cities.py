from geopy.geocoders import Nominatim
import math


def city_distance(city_one, city_two):
    r = 6371
    lat = math.radians(abs(city_two.latitude - city_one.latitude))
    long = math.radians(abs(city_two.longitude - city_one.longitude))

    a = math.sin(lat / 2) * math.sin(lat / 2)
    a += math.cos(math.radians(city_one.latitude)) * math.cos(math.radians(city_two.latitude))
    a *= math.sin(long / 2) * math.sin(long / 2)

    a = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    a *= r

    return a


locator = Nominatim()

city_one = input("Input the name of the first city:\n")
city_two = input("Input the name of the second city:\n")

city_one = locator.geocode(city_one)
city_two = locator.geocode(city_two)

print("Distance is:", city_distance(city_one, city_two))
