#TODO

class Account:
    def __init__(self, balance, acc_id):
        self.balance = balance
        self.acc_id = acc_id

    def deposit(self, deposit):
        self.balance += deposit

    def withdraw(self, withdraw):
        self.balance -= withdraw

    def get_fees(self):
        if self.balance < 0:
            return f'You have accumulated {abs(self.balance)} in unpaid fees.\n' \
                   f'(Fees are automatically paid upon deposit.)'


class CheckingAccount(Account):
    def __init__(self, balance, overdraft_protection, acc_id):
        super().__init__(balance, acc_id)
        self.overdraft_protection = overdraft_protection

    def withdraw(self, withdraw):
        if withdraw > self.balance and self.overdraft_protection:
            raise InvalidWithdrawException()
        else:
            super().withdraw(withdraw)


class SavingsAccount(Account):
    def __init__(self, balance, acc_id):
        super().__init__(balance, acc_id)
        self.withdraw_limit = 6
        self.withdraw_count = 0

    def withdraw(self, withdraw):
        if withdraw > self.balance:
            raise Exception()
        elif self.withdraw_count == self.withdraw_limit:
            raise Exception()
        else:
            super().withdraw(withdraw)
            self.withdraw_count += 1

    def withdraw_limits(self):
        return f'Withdraw limit: {self.withdraw_limit}\nWithdraw count: {self.withdraw_count}'


class BuisnessAccount(Account):
    def __init__(self, balance, acc_id):
        super().__init__(balance, acc_id)
        self.employees = {}

    def withdraw(self, withdraw):
        if withdraw > self.balance:
            raise Exception()
        else:
            super().withdraw(withdraw)

    def add_employee(self, name, pay):
        self.employees[name] = (pay)

    def pay_payroll(self):
        for pay in self.employees.values():
            self.balance -= pay


